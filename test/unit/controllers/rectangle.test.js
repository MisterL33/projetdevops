//   describe("GET /rectangle/2,5,4,6,4,2", function() {
//     it('should return airmax = 12', function() {
//       return server.run('/rectangle/2,5,4,6,4,2').then((res) => {
//         res.statusCode.should.equal(200);
//         console.log('');
//         res.resultat.airmax.should.equal(12)
//       })
//     })
// })
describe("GET /rectangle/2,5,4,6,4,v", function () {
    it(`should reject when the array parameter is not a number`, function () {
        return server.run('/rectangles/2,5,4,6,4,v').then((res) => {
            res.statusCode.should.equal(400);
            res.result.message.should.equal('Les paramètres doivent être des nombres !');
        })
    })
})
describe("GET /rectangle/8/v", function () {
    it(`should reject when the parameters are not a number`, function () {
        return server.run('/rectangles/8/v').then((res) => {
            res.statusCode.should.equal(400);
            res.result.message.should.equal('Hauteur doit etre un nombre !');
        })
    })
})
describe("GET /rectangle/v/5", function () {
    it(`should reject when the parameters are not a number`, function () {
        return server.run('/rectangles/v/5').then((res) => {
            res.statusCode.should.equal(400);
            res.result.message.should.equal('Largeur doit etre un nombre !');
        })
    })
})