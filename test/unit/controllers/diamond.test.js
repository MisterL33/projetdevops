describe("Module Diamond", () => {
  describe("GET /diamond", function() {
    it('should redirect to /diamond/form', function() {
      return server.run('/diamond/form')
    })
  })

  describe("GET /diamond/new", function() {
    it('should show a form with an input and a submit', function() {
      return server.run('/diamond/showForm')
    })
  })

  describe("POST /diamond/v", function() {
    it(`should reject when width parameter is not a number`, function() {
      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: 'v' }
      }).then(function(res) {
        res.statusCode.should.equal(400)
        res.result.message.should.equal('Width doit etre un nombre !')
      })
    })
  })


  describe("POST /diamond/4", function() {
    it(`should reject when width parameter is not odd`, function() {
      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: '4' }
      }).then(function(res) {
        res.statusCode.should.equal(400)
        })
      })
    })


    testDiamonds = [{
      width: 1,
      result: '+'
    }, {
      width: 3,
      result: [
        '.+.',
        '+++',
        '.+.'
      ].join("\n")
    }, {
      width: 5,
      result: [
        '..+..',
        '.+++.',
        '+++++',
        '.+++.',
        '..+..'
      ].join("\n")
    }]



    // C'est possible de créér des tests unitaires de façon dynamique
    testDiamonds.forEach(function(diamond){
      it(`should show correct result on width = ${diamond.width}`, function() {
        return server.run({
          method: 'post',
          url: '/diamond',
          payload: { width: diamond.width }
        }).then(function(res) {
          res.statusCode.should.equal(200)
          res.result.should.equal(diamond.result)
        })
      })
    })


    ReversetestDiamonds = [{
      width: -1,
      result: '.'
    }, {
      width: -3,
      result: [
        '+.+',
        '...',
        '+.+'
      ].join("\n")
    }, {
      width: -5,
      result: [
        '++.++',
        '+...+',
        '.....',
        '+...+',
        '++.++'
      ].join("\n")
    }]
    
    ReversetestDiamonds.forEach(function(diamond){
      it(`should invert + and . when given width is negative = ${diamond.width}`, function() {
        return server.run({
          method: 'post',
          url: '/diamond',
          payload: { width: diamond.width }
        }).then(function(res) {
          res.statusCode.should.equal(200)
          res.result.should.equal(diamond.result)
        })
      })
    })

})
