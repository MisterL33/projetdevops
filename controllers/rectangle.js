const Boom = require('boom');

module.exports.generate = function (request, reply) {
    value = request.params.numberArray;
    tabValue = value.split(',');
    largeur = Object.keys(tabValue).length;

    hauteur = 0;
    
    for(i = 0; i < largeur; i++)
    {
        if (isNaN(tabValue[i])) {
            return reply(Boom.badRequest('Les paramètres doivent être des nombres !'));
        }

    }
    for (i = 0; i < largeur; i++) {
        if (Number(tabValue[i]) > hauteur) {
            hauteur = Number(tabValue[i]);
        }
    }
    result = [];
    compteurResult = 0;
    // Parcours hauteur
    for (i = hauteur; i > 0; i--) {
        // Parcours largeur
        var resultTab = ""
        resultTab += i;
        tabValue.forEach(function (value) {
            if (value >= i) {
                resultTab += '#'
            }
            else {
                resultTab += ' '
            }
        }, this);
        result.push(resultTab);
        compteurResult++;
    }
    var compteurLargeur = 1;
    var resultTab = "+";
    tabValue.forEach(function (element) {
        resultTab += compteurLargeur;
        compteurLargeur++;
    }, this);


    result.push(resultTab);
    airmax = findAirEnAnglais(tabValue);
    result.push("Ceci est notre airmax : ");
    result = result.join("\n");
    reply(result + airmax);
}




module.exports.generatev2 = function (request, reply) {
    largeur = Number(request.params.largeur);

    hauteur = Number(request.params.hauteur);
    if (isNaN(largeur)) {
        return reply(Boom.badRequest('Largeur doit etre un nombre !'));
    }
    if (isNaN(hauteur)) {
        return reply(Boom.badRequest('Hauteur doit etre un nombre !'));
    }
    result = [];
    tabValue = [];
    for (i = 0; i < largeur; i++) {
        random = Math.floor((Math.random() * hauteur) + 0);
        tabValue.push(random);
    }

    compteurResult = 0;
    // Parcours hauteur
    for (i = hauteur; i > 0; i--) {
        // Parcours largeur
        var resultTab = ""
        resultTab += i;
        tabValue.forEach(function (value) {
            if (value >= i) {
                resultTab += '#'
            }
            else {
                resultTab += ' '
            }
        }, this);
        result.push(resultTab);
        compteurResult++;
    }
    var compteurLargeur = 1;
    var resultTab = "+";
    tabValue.forEach(function (element) {
        resultTab += compteurLargeur;
        compteurLargeur++;
    }, this);


    result.push(resultTab);
    airmax = findAirEnAnglais(tabValue);
    result.push("Ceci est notre airmax : " + airmax);
    result = result.join("\n");
    reply("<pre>" + result + "</pre>") // pas besoin de régler la police, la balise pre utilise de base la police courrier
}

function findAirEnAnglais(tabValue) {
    airmax = 0;
    var grossevalue1 = 0;
    for (i = 0; i < tabValue.length; i++) {
        var value1 = Number(tabValue[i]);
        var next = i + 1;
        var value2 = Number(tabValue[next]);
        var airactuel = value1;
        var airlongactuel = value1;
        grossevalue1 = analysearea(value1, grossevalue1);
        if (value1 > 0 && value2 > 0) {
            if (value1 <= value2) {
                while (value1 <= value2) {
                    airactuel += value1;
                    next++;
                    value2 = Number(tabValue[next]);
                }
                airmax = analysearea(airactuel, airmax)
            }


            jump = 1;
            minfound = value2;
            while (value2 > 0) {
                jump++;
                next++;
                if (value2 < minfound) {
                    minfound = value2;
                }
                value2 = Number(tabValue[next]);
            }
            airactuel = minfound * jump;
            airmax = analysearea(airactuel, airmax);

        }
    }
    airmax = analysearea(grossevalue1, airmax);
    console.log('Air maximum : ', airmax);
    return airmax;
}

function analysearea(airactuel, airmax) {
    if (airactuel > airmax) {
        return airactuel;
    }
    else {
        return airmax;
    }
}