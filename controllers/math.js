// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom')
const history = require('../lib/historyDao.js')

module.exports.getAdd = function(request, reply) {
    // parseFloat convert anything to either a float number or NaN
    var term1 = parseFloat(request.params.term1)
    var term2 = parseFloat(request.params.term2)
    var result = term1 + term2
    var operande = "+"

    if (isNaN(term1) || isNaN(term2)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }
    history.add(term1, operande, term2)
    reply(result.toFixed(2))
}

module.exports.getDivide = function(request, reply) {
    dividend = parseFloat(request.params.dividend)
    divisor = parseFloat(request.params.divisor)
    operande = "/"
    if (isNaN(dividend) || isNaN(divisor)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }

    if (divisor === 0) {
        return reply(Boom.badRequest('Divisor can not be 0.'))
    }
    history.add(dividend, operande, divisor)
    reply(dividend / divisor)
}

module.exports.getSubstract = function(request, reply) {
    firstnumber = parseFloat(request.params.firstnumber)
    secondnumber = parseFloat(request.params.secondnumber)
    operande = "-"

    if (isNaN(firstnumber) || isNaN(secondnumber)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }
    history.add(dividend, operande, divisor)
    reply(firstnumber - secondnumber)
}

module.exports.getHistory = function(request, reply) {

    history.list(lst => {
      reply(lst)
    })
}

module.exports.getMultiply = function(request, reply) {
    // parseFloat convert anything to either a float number or NaN
    term1 = parseFloat(request.params.term1)
    term2 = parseFloat(request.params.term2)

    if (isNaN(term1) || isNaN(term2)) {
        return reply(Boom.badRequest('At least one of the term is not a number.'))
    }

    reply(term1 * term2)
}
