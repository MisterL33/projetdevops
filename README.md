# README #

Projet DevOps

### Rapport ###

#### Repartition des tâches ####

* Sebastien BOURSIER : gitFlow, hotfixes, support
* Guillaume MASSON : dev feature
* Duncan Le goff : dev feature
* Laurent Julienne : dev feature
* Lucas Février : pipelines, ansible
* Valentin Constanty : ansible, serverSpec


#### Etat du projet ####

##### Ce qui est fait #####
 * V0.2.0 (DO_27)
 * V0.2.1 (DO_79)
 * V0.3.0 (DO_51, DO_52)
 * V0.4.0 (DO_53)
 * V1.0.0 (DO_60, DO_61)
 * V1.1.0 (DO_150, DO_155)
 * V1.1.1 (DO_172)
 * V1.2.0 (DO_161)

##### Ce qu'il reste à faire #####

 * DO_28, en chantier depuis le debut par Valentin Constanty. Lucas Fevrier a essayer de prendre la suite mais n'as pas réussi.
 * DO_29, en attente de la DO_28
 * DO_30, Lucas Fevrier à trouver la technique de l'API, mais en attente de la DO_29
 * DO_35, en attente de l'héroique R&D de Valentin Constanty.

#### Capitalisation ####

##### Ce qui s'est mal passé #####

L'investissement de la majorité de l'équipe a du se prendre des vacances aux Bahamas, car non présente.

La partie ops du projet est dans le néant.

Les devs n'ont pas compris que leur tâche n'était que de la fioriture pour nous faire comprendre les processus DevOps.

##### Ce qui s'est bien passé #####

Les processus GitFlow. (jusqu'à ce que Guillaume Masson et Duncan LeGoff se chargent de la dernière release et commit directe sur master...)

#####Ce qu'il aurait fallu faire #####

<MODE_COUP_DE_GEULE>
  (sébastien, avis perso) : Que je fasse tout, mais franchement j'en ai marre d'avoir des équipes comme ça...
  J'ai fait ma part et c'est tout, pour une fois.
  Je suis sur qu'ils ne liront même pas ce rapport... (coucou si vous le faite!)
</MODE_COUP_DE_GEULE>
