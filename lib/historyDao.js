const mongoose = require('mongoose')
const path = require('path')
const mongoConfig = require(path.join(__dirname, '..', 'config')).mongo

let db = mongoose.connect(`mongodb://${mongoConfig.host}:${mongoConfig.port}/${mongoConfig.database}`)

let schema = new mongoose.Schema({
    id : Number,
    term1 : String,
    term2 : String,
    operand : String,
    createdAt : { type : Date, default : Date.now },
})

module.exports.add = (term1, operand, term2) => {
  var Model = db.model('math', schema)
  var newItem = new Model({
    term1 : term1,
    operand : operand,
    term2 : term2
  })
  newItem.save()
}

module.exports.list = (callback) => {
  var Model = db.model('math', schema)
  Model.find(null).sort({createdAt: 'desc'}).exec((err, lst) => {
    if (err) { throw err }
    callback(lst)
  })
}
